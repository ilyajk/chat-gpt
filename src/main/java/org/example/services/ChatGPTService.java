package org.example.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpPost;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChatGPTService {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final String API_URL = "https://api.openai.com/v1/completions";
    private static final String CONTEXT_QUESTION = "О чем написано далее? ";
    @Value("${openai.max_token}")
    private Integer MAX_TOKEN;
    @Value("${openai.total_token}")
    private Integer TOTAL_TOKEN;
    @Value("${openai.temperature}")
    private Double TEMPERATURE;
    @Value("${openai.api_key}")
    private String API_KEY;
    @Value("${openai.model}")
    private String MODEL;

    public ResponseEntity<?> fileContext(MultipartFile file) {
        StringBuilder stringBuilder = new StringBuilder();
        List<String> extractedFile = extractFile(file);
        String content = "";
        HttpPost httpPost = new HttpPost(API_URL);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Authorization", "Bearer " + API_KEY);
        for (String str : extractedFile) {
            //davinci
            String jsonPayload = "{\"model\": \"text-davinci-003\",\"prompt\": \"" + str + "\", \"temperature\": " + TEMPERATURE + ", \"max_tokens\": " + MAX_TOKEN + "}";
            //chat
            //String jsonPayload = "{\"model\": \"" + MODEL + "\",\"messages\": [{\"role\": \"user\", \"content\": \"" + str + "\"}], \"temperature\": " + TEMPERATURE + ", \"max_tokens\": " + MAX_TOKEN + "}";
            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                StringEntity requestEntity = new StringEntity(jsonPayload, StandardCharsets.UTF_8);
                httpPost.setEntity(requestEntity);
                try (CloseableHttpResponse response = httpClient.execute(httpPost)) {
                    HttpEntity responseEntity = response.getEntity();
                    if (responseEntity != null) {
                        String responseString = EntityUtils.toString(responseEntity, StandardCharsets.UTF_8);
                        JsonNode rootNode = objectMapper.readTree(responseString);
                        //content = rootNode.get("choices").get(0).get("message").get("content").asText(); //chat
                        content = rootNode.get("choices").get(0).get("text").asText(); // davinci
                        log.debug(content);
                        stringBuilder.append(content + " ");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return ResponseEntity.ok().body(stringBuilder.toString());
    }

    private List<String> extractFile(MultipartFile multipartFile) {
        List<String> extractedFile = new ArrayList<>();
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());
        if (extension.equals("docx")) {
            File docx;
            try {
                docx = File.createTempFile(multipartFile.getOriginalFilename(),null);
                InputStream initialStream = multipartFile.getInputStream();
                byte[] buffer = new byte[initialStream.available()];
                initialStream.read(buffer);
                try (OutputStream outStream = new FileOutputStream(docx)) {
                    outStream.write(buffer);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            try {
                FileInputStream fis = new FileInputStream(docx.getAbsolutePath());
                XWPFDocument document = new XWPFDocument(fis);
                List<XWPFParagraph> paragraphs = document.getParagraphs();
                for (XWPFParagraph para : paragraphs) {
                    extractedFile.add(para.getText());
                }
                fis.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            ArrayList<String> extractedCombinedFile = new ArrayList <>();
            int index = 0;
            for (String str : extractedFile) {
                log.debug(String.valueOf(str.length()));
                if (extractedCombinedFile.isEmpty()) {
                    extractedCombinedFile.add(CONTEXT_QUESTION + str);
                    continue;
                }
                if (str.length() < (TOTAL_TOKEN - MAX_TOKEN - extractedCombinedFile.get(index).length() - CONTEXT_QUESTION.length())) {
                    String temp = extractedCombinedFile.get(index).concat(" " + str);
                    extractedCombinedFile.set(index, temp);
                } else {
                    extractedCombinedFile.add(CONTEXT_QUESTION + str);
                    index++;
                }
            }
            log.debug("Combined Input");
            extractedCombinedFile.forEach(str -> log.debug(String.valueOf(str.length())));
            return extractedCombinedFile;
        }
        return extractedFile;
    }

}
