package org.example.controller;

import lombok.AllArgsConstructor;
import org.example.services.ChatGPTService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@AllArgsConstructor
@RequestMapping("/chatGPT")
public class ChatGPTController {
    private final ChatGPTService chatGPTService;
//@RequestPart("files") MultipartFile file
    @PostMapping("/fileContext")
    public ResponseEntity<?> fileContext(@RequestBody MultipartFile file){
        return chatGPTService.fileContext(file);
    }
}
